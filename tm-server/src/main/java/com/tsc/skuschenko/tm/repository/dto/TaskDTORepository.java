package com.tsc.skuschenko.tm.repository.dto;

import com.tsc.skuschenko.tm.api.repository.dto.ITaskDTORepository;
import com.tsc.skuschenko.tm.dto.TaskDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public final class TaskDTORepository
        extends AbstractDTORepository<TaskDTO>
        implements ITaskDTORepository {

    public TaskDTORepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void clear(@NotNull String userId) {
        @NotNull final String query =
                "DELETE FROM TaskDto e WHERE e.userId = :userId";
        entityManager.createQuery(query, TaskDTO.class)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public void clearAllTasks() {
        @NotNull final String query = "DELETE FROM TaskDto e";
        entityManager.createQuery(query, TaskDTO.class).executeUpdate();
    }

    @Nullable
    @Override
    public List<TaskDTO> findAll() {
        @NotNull final String query = "SELECT e FROM TaskDto e ";
        return entityManager.createQuery(query, TaskDTO.class)
                .getResultList();
    }

    @Override
    public @Nullable List<TaskDTO> findAllTaskByProjectId(
            @NotNull String userId, @NotNull String projectId
    ) {
        @NotNull final String query =
                "SELECT e FROM TaskDto e WHERE e.userId = :userId AND " +
                        "e.projectId = :projectId";
        return entityManager.createQuery(query, TaskDTO.class)
                .setParameter("projectId", projectId)
                .setParameter("userId", userId).getResultList();
    }

    @Nullable
    @Override
    public List<TaskDTO> findAllWithUserId(@NotNull String userId) {
        @NotNull final String query =
                "SELECT e FROM TaskDto e WHERE e.userId = :userId";
        return entityManager.createQuery(query, TaskDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public TaskDTO findById(@NotNull String id) {
        return entityManager.find(TaskDTO.class, id);
    }

    @Nullable
    @Override
    public TaskDTO findOneById(@NotNull String userId, @NotNull String id) {
        @NotNull final String query =
                "SELECT e FROM TaskDto e WHERE e.userId = :userId AND " +
                        "e.id = :id";
        TypedQuery<TaskDTO> typedQuery =
                entityManager.createQuery(query, TaskDTO.class)
                        .setParameter("id", id)
                        .setParameter("userId", userId);
        return getEntity(typedQuery);
    }

    @Nullable
    @Override
    public TaskDTO findOneByIndex(
            @NotNull String userId, @NotNull Integer index
    ) {
        @NotNull final String query =
                "SELECT e FROM TaskDto e WHERE e.userId = :userId";
        TypedQuery<TaskDTO> typedQuery =
                entityManager.createQuery(query, TaskDTO.class)
                        .setParameter("userId", userId).
                        setFirstResult(index);
        return getEntity(typedQuery);
    }

    @Nullable
    @Override
    public TaskDTO findOneByName(
            @NotNull String userId, @NotNull String name
    ) {
        @NotNull final String query =
                "SELECT e FROM TaskDto e WHERE e.userId = :userId AND " +
                        "e.name = :name";
        TypedQuery<TaskDTO> typedQuery =
                entityManager.createQuery(query, TaskDTO.class)
                        .setParameter("name", name)
                        .setParameter("userId", userId);
        return getEntity(typedQuery);
    }

    @Override
    public void removeOneById(
            @NotNull String userId, @NotNull String projectId
    ) {
        @NotNull final String query =
                "DELETE FROM TaskDto e WHERE e.userId = :userId AND " +
                        "e.projectId = :projectId";
        entityManager.createQuery(query, TaskDTO.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .executeUpdate();
    }

}