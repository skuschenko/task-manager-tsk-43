package com.tsc.skuschenko.tm.service;

import com.tsc.skuschenko.tm.api.service.IPropertyService;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;
import java.util.Properties;

public class PropertyService implements IPropertyService {

    @NotNull
    public static final String FILE_NAME = "application.properties";

    @NotNull
    private static final String APPLICATION_VERSION_KEY = "version";

    @NotNull
    private static final String AUTHOR_EMAIL_KEY = "author.email";

    @NotNull
    private static final String AUTHOR_KEY = "author";

    @NotNull
    private static final String BACKUP_DEFAULT_TIME = "60";

    @NotNull
    private static final String BACKUP_TIME = "backupTime";

    @NotNull
    private static final String FILE_BACKUP_XML = "filePath.backupXml";

    @NotNull
    private static final String FILE_BASE64 = "filePath.base64";

    @NotNull
    private static final String FILE_BINARY = "filePath.binary";

    @NotNull
    private static final String FILE_JAXB_JSON = "filePath.jaxbJson";

    @NotNull
    private static final String FILE_JAXB_XML = "filePath.jaxbXml";

    @NotNull
    private static final String FILE_JSON = "filePath.json";

    @NotNull
    private static final String FILE_SCANNER = "fileScannerTime";

    @NotNull
    private static final String FILE_SCANNER_TIME = "60";

    @NotNull
    private static final String FILE_XML = "filePath.xml";

    @NotNull
    private static final String FILE_YAML = "filePath.yaml";

    @NotNull
    private static final String JDBC_DIALECT = "jdbc.dialect";

    @NotNull
    private static final String JDBC_DRIVER = "jdbc.driver";

    @NotNull
    private static final String JDBC_HBM2DDL = "jdbc.hbm2ddl";

    @NotNull
    private static final String JDBC_PASSWORD = "jdbc.password";

    @NotNull
    private static final String JDBC_SHOW_SQL = "jdbc.showSql";

    @NotNull
    private static final String JDBC_URL = "jdbc.url";

    @NotNull
    private static final String JDBC_USER_NAME = "jdbc.userName";

    @NotNull
    private static final String PASSWORD_ITERATION_DEFAULT = "25456";

    @NotNull
    private static final String PASSWORD_ITERATION_KEY = "password.iteration";

    @NotNull
    private static final String PASSWORD_SECRET_DEFAULT = "356585985";

    @NotNull
    private static final String PASSWORD_SECRET_KEY = "password.secret";

    @NotNull
    private static final String SERVER_HOST = "server.host";

    @NotNull
    private static final String SERVER_PORT = "server.port";

    @NotNull
    private static final String SESSION_CYCLE = "session.cycle";

    @NotNull
    private static final String SESSION_SALT = "session.salt";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        properties.load(ClassLoader.getSystemResourceAsStream(FILE_NAME));
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        if (System.getProperties().contains(APPLICATION_VERSION_KEY)) {
            return System.getProperty(APPLICATION_VERSION_KEY);
        }
        if (System.getenv().containsKey(APPLICATION_VERSION_KEY)) {
            return System.getenv(APPLICATION_VERSION_KEY);
        }
        return properties.getProperty(APPLICATION_VERSION_KEY);
    }

    @NotNull
    @Override
    public String getAuthor() {
        if (System.getProperties().contains(AUTHOR_KEY)) {
            return System.getProperty(AUTHOR_KEY);
        }
        if (System.getenv().containsKey(AUTHOR_KEY)) {
            return System.getenv(AUTHOR_KEY);
        }
        return properties.getProperty(AUTHOR_KEY);
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        if (System.getProperties().contains(AUTHOR_EMAIL_KEY)) {
            return System.getProperty(AUTHOR_EMAIL_KEY);
        }
        if (System.getenv().containsKey(AUTHOR_EMAIL_KEY)) {
            return System.getenv(AUTHOR_EMAIL_KEY);
        }
        return properties.getProperty(AUTHOR_EMAIL_KEY);
    }

    @NotNull
    @Override
    public Integer getBackupTime() {
        return Integer.parseInt(
                Optional.ofNullable(properties.getProperty(BACKUP_TIME))
                        .orElse(BACKUP_DEFAULT_TIME)
        );
    }

    @Nullable
    @Override
    public String getFileBackupPath() {
        return properties.getProperty(FILE_BACKUP_XML);
    }

    @Nullable
    @Override
    public String getFileBase64Path() {
        return properties.getProperty(FILE_BASE64);
    }

    @Nullable
    @Override
    public String getFileBinaryPath() {
        return properties.getProperty(FILE_BINARY);
    }

    @Nullable
    @Override
    public String getFileJsonPath(@Nullable final String className) {
        switch (className) {
            case "jaxB":
                return properties.getProperty(FILE_JAXB_JSON);
            default:
                return properties.getProperty(FILE_JSON);
        }
    }

    @Override
    public @NotNull Integer getFileScannerTime() {
        return Integer.parseInt(
                Optional.ofNullable(properties.getProperty(FILE_SCANNER))
                        .orElse(FILE_SCANNER_TIME)
        );
    }

    @Nullable
    @Override
    public String getFileXmlPath(@Nullable final String className) {
        switch (className) {
            case "jaxb":
                return properties.getProperty(FILE_JAXB_XML);
            default:
                return properties.getProperty(FILE_XML);
        }
    }

    @Nullable
    @Override
    public String getFileYamlPath() {
        return properties.getProperty(FILE_YAML);
    }

    @Nullable
    @Override
    public String getJdbcDialect() {
        return properties.getProperty(JDBC_DIALECT);
    }

    @Nullable
    @Override
    public String getJdbcDriver() {
        return properties.getProperty(JDBC_DRIVER);
    }

    @Nullable
    @Override
    public String getJdbcHbm2ddl() {
        return properties.getProperty(JDBC_HBM2DDL);
    }

    @Nullable
    @Override
    public String getJdbcPassword() {
        return properties.getProperty(JDBC_PASSWORD);
    }

    @Nullable
    @Override
    public String getJdbcShowSql() {
        return properties.getProperty(JDBC_SHOW_SQL);
    }

    @Nullable
    @Override
    public String getJdbcUrl() {
        return properties.getProperty(JDBC_URL);
    }

    @Nullable
    @Override
    public String getJdbcUserName() {
        return properties.getProperty(JDBC_USER_NAME);
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        if (System.getProperties().contains(PASSWORD_ITERATION_KEY)) {
            @NotNull final String value =
                    System.getProperty(PASSWORD_ITERATION_KEY);
            return Integer.parseInt(value);
        }
        if (System.getenv().containsKey(PASSWORD_ITERATION_KEY)) {
            @NotNull final String value =
                    System.getProperty(PASSWORD_ITERATION_KEY);
            return Integer.parseInt(value);
        }
        @NotNull final String value = properties.getProperty(
                PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT
        );
        return Integer.parseInt(value);
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        if (System.getProperties().contains(PASSWORD_SECRET_KEY)) {
            return System.getProperty(PASSWORD_SECRET_KEY);
        }
        if (System.getenv().containsKey(PASSWORD_SECRET_KEY)) {
            return System.getenv(PASSWORD_SECRET_KEY);
        }
        return properties.getProperty(
                PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT
        );
    }

    @Nullable
    @Override
    public String getServerHost() {
        return properties.getProperty(SERVER_HOST);
    }

    @Nullable
    @Override
    public String getServerPort() {
        return properties.getProperty(SERVER_PORT);
    }

    @Nullable
    @Override
    public String getSessionCycle() {
        return properties.getProperty(SESSION_CYCLE);
    }

    @Nullable
    @Override
    public String getSessionSalt() {
        return properties.getProperty(SESSION_SALT);
    }

}
