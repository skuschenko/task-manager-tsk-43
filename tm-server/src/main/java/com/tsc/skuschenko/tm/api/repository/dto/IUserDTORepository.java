package com.tsc.skuschenko.tm.api.repository.dto;

import com.tsc.skuschenko.tm.dto.UserDTO;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IUserDTORepository extends IAbstractDTORepository<UserDTO> {

    void clear();

    @Nullable List<UserDTO> findAll();

    @Nullable
    @SneakyThrows
    UserDTO findByEmail(@NotNull String email);

    @Nullable
    UserDTO findById(@NotNull String id);

    @Nullable
    @SneakyThrows
    UserDTO findByLogin(@NotNull String login);

    @NotNull
    @SneakyThrows
    UserDTO lockUserByLogin(@NotNull String login);

    @Nullable
    @SneakyThrows
    UserDTO removeByLogin(@NotNull String login);

    void removeOneById(@NotNull String id);

    @NotNull
    @SneakyThrows
    UserDTO setPassword(
            @NotNull String userId, @NotNull String password
    );

    @NotNull
    @SneakyThrows
    UserDTO unlockUserByLogin(@NotNull String login);
}
