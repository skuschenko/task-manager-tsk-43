package com.tsc.skuschenko.tm.service;


import com.tsc.skuschenko.tm.api.service.IConnectionService;
import com.tsc.skuschenko.tm.api.service.IPropertyService;
import com.tsc.skuschenko.tm.dto.ProjectDTO;
import com.tsc.skuschenko.tm.dto.SessionDTO;
import com.tsc.skuschenko.tm.dto.TaskDTO;
import com.tsc.skuschenko.tm.dto.UserDTO;
import com.tsc.skuschenko.tm.exception.system.ConnectionFailedException;
import com.tsc.skuschenko.tm.model.Project;
import com.tsc.skuschenko.tm.model.Session;
import com.tsc.skuschenko.tm.model.Task;
import com.tsc.skuschenko.tm.model.User;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class ConnectionService implements IConnectionService {

    @NotNull
    private final EntityManagerFactory entityManagerFactory;
    @NotNull
    private final IPropertyService propertyService;

    @NotNull

    public ConnectionService(@NotNull IPropertyService propertyService) {
        this.propertyService = propertyService;
        this.entityManagerFactory = getHibernateSqlFactory();
    }

    @NotNull
    private Map<String, String> getConnectionProperty() {
        @NotNull final Map<String, String> properties =
                new HashMap<String, String>();
        @Nullable final String driver = propertyService.getJdbcDriver();
        Optional.ofNullable(driver).orElseThrow(() ->
                new ConnectionFailedException("Driver")
        );
        @Nullable final String userName = propertyService.getJdbcUserName();
        Optional.ofNullable(userName).orElseThrow(() ->
                new ConnectionFailedException("UserName")
        );
        @Nullable final String password = propertyService.getJdbcPassword();
        Optional.ofNullable(password).orElseThrow(() ->
                new ConnectionFailedException("Password")
        );
        @Nullable final String url = propertyService.getJdbcUrl();
        Optional.ofNullable(url).orElseThrow(() ->
                new ConnectionFailedException("Url")
        );
        @Nullable final String dialect = propertyService.getJdbcDialect();
        Optional.ofNullable(url).orElseThrow(() ->
                new ConnectionFailedException("Dialect")
        );
        @Nullable final String hbm2ddl = propertyService.getJdbcHbm2ddl();
        Optional.ofNullable(url).orElseThrow(() ->
                new ConnectionFailedException("Hbm2ddl")
        );
        @Nullable final String showSql = propertyService.getJdbcShowSql();
        Optional.ofNullable(url).orElseThrow(() ->
                new ConnectionFailedException("ShowSql")
        );
        properties.put("driver", driver);
        properties.put("userName", userName);
        properties.put("password", password);
        properties.put("url", url);
        properties.put("hbm2ddl", hbm2ddl);
        properties.put("dialect", dialect);
        properties.put("showSql", showSql);
        return properties;
    }

    @Override
    @NotNull
    public EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }

    @Override
    @NotNull
    public EntityManagerFactory getHibernateSqlFactory() {
        @NotNull final Map<String, String> properties = getConnectionProperty();
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(
                org.hibernate.cfg.Environment.DRIVER, properties.get("driver")
        );
        settings.put(
                org.hibernate.cfg.Environment.URL, properties.get("url"))
        ;
        settings.put(
                org.hibernate.cfg.Environment.USER, properties.get("userName")
        );
        settings.put(
                org.hibernate.cfg.Environment.PASS, properties.get("password")
        );
        settings.put(
                org.hibernate.cfg.Environment.DIALECT,
                properties.get("dialect")
        );
        settings.put(
                org.hibernate.cfg.Environment.HBM2DDL_AUTO,
                properties.get("hbm2ddl")
        );
        settings.put(
                org.hibernate.cfg.Environment.SHOW_SQL,
                properties.get("showSql")
        );
        @NotNull final StandardServiceRegistryBuilder registryBuilder =
                new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry =
                registryBuilder.build();
        @NotNull final MetadataSources sources = new MetadataSources(registry);
        sources.addAnnotatedClass(ProjectDTO.class);
        sources.addAnnotatedClass(TaskDTO.class);
        sources.addAnnotatedClass(SessionDTO.class);
        sources.addAnnotatedClass(UserDTO.class);
        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(Task.class);
        sources.addAnnotatedClass(Session.class);
        sources.addAnnotatedClass(User.class);
        @NotNull final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

}
