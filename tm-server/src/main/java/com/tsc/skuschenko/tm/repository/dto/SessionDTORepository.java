package com.tsc.skuschenko.tm.repository.dto;

import com.tsc.skuschenko.tm.api.repository.dto.ISessionDTORepository;
import com.tsc.skuschenko.tm.dto.SessionDTO;
import com.tsc.skuschenko.tm.dto.UserDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.EntityManager;
import java.util.List;

public final class SessionDTORepository extends AbstractDTORepository<SessionDTO>
        implements ISessionDTORepository {

    public SessionDTORepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public @Nullable List<SessionDTO> findAll(@NotNull String userId) {
        @NotNull final String query =
                "SELECT e FROM SessionDTO e WHERE e.userId = :userId";
        return entityManager.createQuery(query, SessionDTO.class)
                .setParameter("userId", userId).getResultList();
    }

    @Override
    public SessionDTO findSessionById(SessionDTO session) {
        return entityManager.find(SessionDTO.class, session.getId());
    }

    @Override
    public void removeSessionById(SessionDTO session) {
        @NotNull final String query =
                "DELETE FROM SessionDTO e WHERE e.id = :id";
        entityManager.createQuery(query, UserDTO.class)
                .setParameter("id", session.getId())
                .executeUpdate();
    }

}
